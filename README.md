# Datenprivaten-Webseite
## Webseite
Die Website liegt in ./website und kann mit Jekyll generiert werden.

## Feedgenerator
Der Generator für den RSS-Podcast-Feed liegt in ./feedgenerator und benötigt Node.js und Yarn um generiert zu werden.
