'use strict';

const readMarkdownFile = require('./content-reader.js');

const COPYRIGHT = 'Datenprivaten 2018 - CC BY-SA 4.0';
const POSTS_FOLDER = '../website/_posts';

const builder = require('xmlbuilder');
const root = builder.create('rss', {version: '1.0', encoding: 'UTF-8'});
root.att('version', '2.0');
root.att('xmlns:atom', 'http://www.w3.org/2005/Atom');
root.att('xmlns:content', 'http://purl.org/rss/1.0/modules/content/');
root.att('xmlns:itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd');
root.att('xmlns:sy', 'http://purl.org/rss/1.0/modules/syndication/');

const channel = root.ele('channel');
channel.ele('title', 'Datenprivaten');
channel.ele('atom:link', {
    href: 'http://ubuntupodcast.org/feed/',
    rel: 'self',
    type: 'application/rss+xml',
});
channel.ele('link', 'https://datenprivaten.de');
channel.ele('description', 'Wir sprechen über Privatsphäre und Datenschutz.');
channel.ele('language', 'de-DE');
channel.ele('lastBuildDate', new Date().toUTCString().replace('GMT', '+0000'));
channel.ele('sy:updatePeriod', 'hourly');
channel.ele('sy:updateFrequency', 1);
channel.ele('copyright', COPYRIGHT);
channel.ele('generator', 'https://gitlab.com/datenprivaten/podcast-feed-generator');
channel.ele('itunes:author', 'Die Datenprivaten');
channel.ele('itunes:category', {text: 'Technology'});
channel.ele('itunes:explicit', 'no');
channel.ele('itunes:type', 'episodic');
channel.ele('fyyd:verify', 'wapcF26X4yk8a52ep21s1Gsae1xd5cayq19oevR', {'xmlns:fyyd': 'https://fyyd.de/fyyd-ns/'});

const channelImage = channel.ele('image');
channelImage.ele('url', 'https://datenprivaten.de/feed/logos/2048x2048.png');
channelImage.ele('titel', 'Datenprivaten');
channelImage.ele('link', 'https://datenprivaten.de');
channelImage.ele('width', 2048);
channelImage.ele('height', 2048);

const episode1 = channel.ele('item');
episode1.ele('title', 'Datenprivaten #1 — Willkommen an Deck!');
episode1.ele('link', 'https://datenprivaten.de/2018/06/10/datenprivaten-1.html');
episode1.ele('description', 'Wir sprechen über die Bedeutung von Datenschutz und Microsofts Übernahme von GitHub.');
episode1.ele('pubDate', new Date(Date.UTC(2018, 5, 10, 6, 0)).toUTCString().replace('GMT', '+0000'));
episode1.ele('guid', '2fd22cd7-e0f8-4255-834c-829eedc9bf37', {isPermaLink: 'false'});
episode1.ele('copyright', COPYRIGHT);
episode1.ele('enclosure', {
    url: 'https://datenprivaten.de/feed/mp3-64/episode001.mp3',
    length: 24637275,
    type: 'audio/mpeg',
});
episode1.ele('content:encoded').cdata(readMarkdownFile(`${POSTS_FOLDER}/2018-06-10-datenprivaten-1.markdown`));
episode1.ele('itunes:title', 'Willkommen an Deck!');
episode1.ele('itunes:season', 1);
episode1.ele('itunes:episode', 1);
episode1.ele('itunes:duration', '51:19');
episode1.ele('itunes:episodeType', 'full');
episode1.ele('itunes:keywords', 'Datenschutz,Amsterdam,GitHub,GitLab,Microsoft,NOYB');

const episode2 = channel.ele('item');
episode2.ele('title', 'Datenprivaten #2 — No-Spy Konferenz 2018');
episode2.ele('link', 'https://datenprivaten.de/2018/07/04/no-spy-konferenz-2018.html');
episode2.ele('description', 'Wir sprechen über die Themen der siebten No-Spy Konferenz. Eingeleitet wurde das Ereignis mit einem Anruf von GNU-Begründer Richard Stallman.');
episode2.ele('pubDate', new Date(Date.UTC(2018, 6, 4, 6, 0)).toUTCString().replace('GMT', '+0000'));
episode2.ele('guid', '3e4853b4-d598-42e6-b035-c3291d016800', {isPermaLink: 'false'});
episode2.ele('copyright', COPYRIGHT);
episode2.ele('enclosure', {
    url: 'https://datenprivaten.de/feed/mp3-64/episode002.mp3',
    length: 30884301,
    type: 'audio/mpeg',
});
episode2.ele('content:encoded').cdata(readMarkdownFile(`${POSTS_FOLDER}/2018-07-04-no-spy-konferenz-2018.markdown`));
episode2.ele('itunes:title', 'No-Spy Konferenz 2018');
episode2.ele('itunes:season', 1);
episode2.ele('itunes:episode', 2);
episode2.ele('itunes:duration', '1:04:20');
episode2.ele('itunes:episodeType', 'full');
episode2.ele('itunes:keywords', 'Datenschutz,Privatsphäre,No-Spy,Stallman,Nextcloud,SaveYourInternet,Datenvermeidung');

console.log(channel.end({ pretty: true}));
