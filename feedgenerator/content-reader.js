'use strict';

const fs = require('fs');
const marked = require('marked');

const header = /^---\n[\w\W]*\n---\n/;
const audioPlayer = /{% .* %}/g;

const readMarkdownFile = (path) => {
    let input = fs.readFileSync(path, 'utf8');
    // Remove some elements
    input = input.replace(header, '');
    input = input.replace(audioPlayer, '');
    // Convert the Markdown to HTML
    return marked(input);
};

module.exports = readMarkdownFile;
