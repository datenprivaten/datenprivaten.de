module Jekyll
  class AudioElementTag < Liquid::Tag

    def initialize(tag_name, text, tokens)
      super
      @file = text.strip
    end

    def render(context)
      "<audio preload=\"none\" style=\"width: 100%;\" controls=\"controls\">
        <source type=\"audio/mpeg\" src=\"#{@file}\" />
        <a href=\"#{@file}\">#{@file}</a>
      </audio>"
    end
  end
end

Liquid::Template.register_tag('audio_element', Jekyll::AudioElementTag)
