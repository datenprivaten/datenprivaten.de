---
layout: post
title: "Datenprivaten #1 — Willkommen an Deck!"
date: 2018-06-10 06:00:00 -0000
categories: Podcast Episode Datenschutz Amsterdam GitHub GitLab Microsoft NOYB

---
Wir reden über Datenschutz, digitale Selbstbestimmung und Privatsphäre in der aktuellen Zeit. Außerdem diskutieren wir Microsofts Übernahme von GitHub.

{% audio_element https://datenprivaten.de/feed/mp3-64/episode001.mp3 %}

## Datenschutz
* Neuerungen der DSGVO:
  * [https://netzpolitik.org/2016/eu-parlament-beschliesst-datenschutzgrundverordnung/](https://netzpolitik.org/2016/eu-parlament-beschliesst-datenschutzgrundverordnung/)

* SourceForge & GIMP:
  * [https://www.gimp.org/news/2015/05/27/sourceforge-what-the/](https://www.gimp.org/news/2015/05/27/sourceforge-what-the/)
* ';--have i been pwned?
  * [';--have i been pwned?](https://haveibeenpwned.com/) überprüft, ob Email-Adressen in einem bekannten Datenleck verwickelt waren. Dies ist ein guter Hinweis darauf, dass das Passwort und andere persönliche Daten zu diesem Zeitpunkt teilweise oder vollständig aufgedeckt wurden. Sobald ein Datenleck zu einer Email-Adresse bekannt wird, sollte das Passwort auf dem betroffenen Dienst geändert werden.

## Microsoft übernimmt GitHub
* GitHub nach GitLab Import Statistiken:
  * [https://monitor.gitlab.net/dashboard/db/github-importer?orgId=1](https://monitor.gitlab.net/dashboard/db/github-importer?orgId=1)

* GitHub’s Nachricht über die Einführung/Unterstützung des Co-Autoren Headers in Git:
  * [https://help.github.com/enterprise/2.13/user/articles/creating-a-commit-with-multiple-authors/](https://help.github.com/enterprise/2.13/user/articles/creating-a-commit-with-multiple-authors/)

* NOYB — My Privacy is none of your Business
  * [https://noyb.eu/](https://noyb.eu/)
