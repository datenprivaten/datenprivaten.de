---
layout: post
title: "Datenprivaten #2 — No-Spy Konferenz 2018"
date: 2018-07-04 06:00:00 -0000
categories: Podcast Episode Datenschutz Privatsphäre No-Spy Stallman Nextcloud SaveYourInternet Datenvermeidung

---
Wir sprechen über die Themen der siebten No-Spy Konferenz. Eingeleitet wurde das Ereignis mit einem Anruf von GNU-Begründer Richard Stallman.

{% audio_element https://datenprivaten.de/feed/mp3-64/episode002.mp3 %}

Auf der [No-Spy Konferenz](https://no-spy.org/) finden sich Interessierte zusammen und sprechen über Datenschutz und Datenvermeidung. Wir berichten von der siebten Konferenz, welche vom 15. bis 17.06.2018 in Stuttgart stattfand.

* Fernschalte von [Richard Stallman](https://en.wikipedia.org/wiki/Richard_Stallman)
  * Vortrag über ["Internet of Stings"](https://stallman.org/glossary.html#internetofstings) und weitere Unsicherheiten im Alltag.
  * Überwachung schränkt Demokratie ein
  * Selbst "Privacy by Design" schränkt Überwachung nicht genügend ein
  * Juristische Schutzmaßnahmen sind weitaus wichtiger als alle technischen!
  * Man kämpft nicht alleine, es gibt mehr Leute, denen Privatspäre wichtig ist
  * Wenn man will, kann man Systeme auch datensparsam bauen
* Off-topic EU-Artikel 13 und Upload-Filter
  * [#SaveYourInternet](http://saveyourinternet.eu/)
  * Web Platformen sollen für illegale Inhalte ihrer Nutzer haften.
  * Praktisch können sich die Platformen nur mit automatischen Upload-Filtern schützen.
  * [YouTube Content ID](https://support.google.com/youtube/answer/2797370?hl=en) verwendet schon seit Jahren YouTube-ID um urheberrechtlich geschüzte Video- und Audioanteile erkennen soll.
* Föderation mit Nextcloud
  * [Folien: "Wie Nextcloud Merkel vor Trump beschützt" von Björn Schießle](https://www.schiessle.org/articles/2018/05/31/wie-nextcloud-merkel-vor-trump-besch%C3%BCtzt---oder-wie-eine-freie-selbst-gehostete-cloud-deine-daten-sch%C3%BCtzt/)
  * Die Bundescloud basiert auf Nextcloud.
  * Nextcloud arbeitet weiter an einem Föderationsprotokoll. Dieser soll als Standard etabliert werden um zwischen verschiedenen Cloudanwendungen Daten austauschen zu können.
  * Nextcloud föderiert aktuell mit ownCloud und [pydio](https://pydio.com/).
  * Kann man geschlossene Dienste zur Föderation zwingen?
    * Forderung aus der Politik: Whatsapp muss föderieren
* Warum ist Datenschutz wichtig?
  * Dies war das Fragestellung einer Diskussion auf der Konferenz.
  * In einigen Ländern ist die Verschlüsseln von Daten verboten. Dies hat einen negativen Effekt auf die Meinungsfreiheit.
  * Diskriminierung durch selbsterstellte Daten zum Beispiel bei Krankenversicherungen durch Gesundheitsdaten
  * Bei Versicherungen gibt es ein Machtspiel zwischen einer akkuraten Risikoerfassung und der Privatsphäre der Individuen.
  * Bewusstsein für gesammelte Daten? (Google klopft an die Haustür und man gibt ihnen alles)
  * Unabsichtliche Datenweitergabe über andere - andere in Gefahr bringen durch unabsichtiges Uploaden (z.B. App-Backups, Kontaktbuch- / SMS-Backup)
    * Willkürliches Sammeln von Daten durch Geheimdienste/Ermittlungsbehörden nicht sachdienlich
    * [Vortrag von Peter Welchering](http://www.deutschlandfunk.de/daten-ohne-akten-rueckhalt-warum-staatsschutzdateien-oft.684.de.html?dram:article_id=413280)
    * Zuviele Daten entstehen "einfach so", zum Beispiel auf der Basis von Mutmaßungen.
  * Korrelation ungef. Daten können gefährliche Daten erzeugen
  * Einbruchsgefahr (Rob my home)
  * Jeder hat etwas zu verbergen (Vorhänge, E-Mail Passwort)
* Cryptoparties und das Einsteigerproblem
  * Gezeigt wurde unter anderem ein Screenshot der [Webseite von Signal](https://www.signal.org/) und einer von OTR (Jabber) – Welten liegen dazwischen im Bezug auf Einsteigerfreundlichkeit.
  * Um Verschlüsselung massentauglich zu machen, muss sie zugänglich sein, bspw.: Signal, Let's Encrypt
  * Threat model ist immer relevant. Leute auf PGP bringen ist schön, aber wenn sie frustriert abspringen, ist das Vorhaben gescheitert.
